package cl.mcerda.basic.api;

import java.nio.charset.StandardCharsets;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import cl.mcerda.basic.exceptions.BusinessException;
import cl.mcerda.basic.service.CommandApiService;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.vertx.UniHelper;
import io.vertx.core.Vertx;

@Path("/command")
public class CommnandApiResource {
	private static final Logger log = Logger.getLogger(CommnandApiResource.class);

	@Inject
	private Vertx vertx;

	@Inject
	private CommandApiService commandApiService;

	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public Uni<String> hello() {
		log.info("hello");

		return UniHelper.toUni(vertx.fileSystem().readFile("lorem.txt"))
				.onItem()
				.transform(content -> content.toString(StandardCharsets.UTF_8));

	}
	
	

	@GET
	@Path("/send/{message}")
	@Produces(MediaType.TEXT_PLAIN)
	public Uni<String> send(String message) throws BusinessException {
		log.info("send");

		return UniHelper.toUni(commandApiService.getMessage(message))
				.onItem()
				.transform(content -> content).onFailure().transform(t -> new Exception("algo pasa"));

	}
}