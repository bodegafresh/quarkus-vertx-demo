package cl.mcerda.basic.service.impl;

import javax.ws.rs.core.Response;

import cl.mcerda.basic.exceptions.BusinessException;
import cl.mcerda.basic.service.InternalService;
import io.vertx.core.Future;

public class InternalServiceImpl implements InternalService {

	@Override
	public Future<String> getRealValue(String uuid) throws BusinessException {
		
		throw new BusinessException(Response.Status.EXPECTATION_FAILED, "exception", "El mensaje para un error", "Mayor detalle del error si es necesario");
	}

}
