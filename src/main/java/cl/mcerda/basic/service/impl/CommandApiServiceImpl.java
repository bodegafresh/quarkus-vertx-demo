package cl.mcerda.basic.service.impl;

import cl.mcerda.basic.exceptions.BusinessException;
import cl.mcerda.basic.service.CommandApiService;
import cl.mcerda.basic.service.InternalService;
import io.vertx.core.Future;
import io.vertx.core.Promise;

public class CommandApiServiceImpl implements CommandApiService {

	
	private final InternalService internalService;
	
	public CommandApiServiceImpl(InternalService internalService) {
		this.internalService = internalService;
	}

	@Override
	public Future<String> getMessage(String uuid) throws BusinessException {
		Promise<String> promise = Promise.promise();
		
		internalService.getRealValue(uuid).onComplete(ar -> {
			if (ar.succeeded()) {
				promise.complete(ar.result());
			} else {
				promise.fail(ar.cause());
			}
		});
		
		return promise.future();
		
	}

}

