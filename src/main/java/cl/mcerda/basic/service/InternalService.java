package cl.mcerda.basic.service;

import cl.mcerda.basic.exceptions.BusinessException;
import io.vertx.core.Future;

public interface InternalService {
	
	Future<String> getRealValue(String uuid) throws BusinessException;

}
