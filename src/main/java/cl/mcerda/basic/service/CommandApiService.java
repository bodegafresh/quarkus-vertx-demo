package cl.mcerda.basic.service;

import cl.mcerda.basic.exceptions.BusinessException;
import io.vertx.core.Future;

public interface CommandApiService {
	
	Future<String> getMessage(String uuid) throws BusinessException;

}
