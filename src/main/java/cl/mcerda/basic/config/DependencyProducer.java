package cl.mcerda.basic.config;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import cl.mcerda.basic.service.CommandApiService;
import cl.mcerda.basic.service.InternalService;
import cl.mcerda.basic.service.impl.CommandApiServiceImpl;
import cl.mcerda.basic.service.impl.InternalServiceImpl;

@Singleton
public class DependencyProducer {
	
	@Produces
	@Singleton
	@Default
	public InternalService internalService() {
		return new InternalServiceImpl();
	} 
	
	@Produces
	@Singleton
	@Default
	public CommandApiService commandApiService(InternalService internalService) {
		return new CommandApiServiceImpl(internalService);
	} 
	
	

}
