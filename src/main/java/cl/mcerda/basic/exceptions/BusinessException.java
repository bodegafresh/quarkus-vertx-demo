package cl.mcerda.basic.exceptions;

import javax.ws.rs.core.Response;

import lombok.Getter;

@Getter
public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Response.Status httpStatus;
	private final String detail;
	private final String messaje;

	public BusinessException(Response.Status httpStatus, String error, String messaje, String detail) {
		super(error);
		this.httpStatus = httpStatus;
		this.detail = detail;
		this.messaje = messaje;
	}

}
