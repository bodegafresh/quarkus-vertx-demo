package cl.mcerda.basic.exceptions;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

@Singleton
public class ExceptionHandler {

	@ServerExceptionMapper	
	public Response mapException(BusinessException exception) {

		return Response.status(exception.getHttpStatus())
				.type(MediaType.APPLICATION_JSON_TYPE)
				.entity(ErrorResponseDTO.builder()
						.detail(exception.getDetail())
						.error(exception.getMessage())
						.messaje(exception.getMessaje())
						.build())
				.build();
	}

}
