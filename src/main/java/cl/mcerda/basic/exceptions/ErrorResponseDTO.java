package cl.mcerda.basic.exceptions;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorResponseDTO implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	private String error;
	private String messaje;
	private String detail;

}
