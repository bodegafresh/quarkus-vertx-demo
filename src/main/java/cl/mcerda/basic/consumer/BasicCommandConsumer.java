package cl.mcerda.basic.consumer;

import java.util.concurrent.CompletionStage;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.jboss.logging.Logger;

import cl.mcerda.basic.dto.CommandDTO;
import io.smallrye.reactive.messaging.kafka.api.IncomingKafkaRecordMetadata;

@ApplicationScoped
public class BasicCommandConsumer {

	private static final Logger log = Logger.getLogger(BasicCommandConsumer.class);

	@Incoming("send-command-topic")
	public CompletionStage<Void> consume(Message<CommandDTO> msg) {

		var metadata = msg.getMetadata(IncomingKafkaRecordMetadata.class).orElseThrow();
		log.info(metadata);
		CommandDTO mensaje = msg.getPayload();
		log.info("En consumer: " + mensaje);
		
		return msg.ack();
	}
	
	/*@Incoming("send-command-topic")
	public CompletionStage<Void> consume(Message<String> msg) {

		var metadata = msg.getMetadata(IncomingKafkaRecordMetadata.class).orElseThrow();
		log.info(metadata);
		String mensaje = msg.getPayload();
		log.info("En consumer: " + mensaje);
		
		return msg.ack();
	}*/
}
